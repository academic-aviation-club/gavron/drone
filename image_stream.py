import imutils
import logging
import os
import socket

import imagezmq
from imutils.video import VideoStream

import config

logger = logging.getLogger(__name__)

if 'MOCK_CAMERA' not in os.environ:
    class ImageStream:
        def __init__(self, image_server=config.IMAGE_SERVER_URL):
            self.sender = imagezmq.ImageSender(connect_to=image_server)
            self.rpi_name = socket.gethostname()  # send RPi hostname with each image
            self.picam = VideoStream(usePiCameraa=True, resolution=config.VIDEO_RESOLUTION)
            self.stream = self.picam.start()

        def take_and_upload_photo(self, flight_id="test"):
            if flight_id == "test":
                logger.warning("Using default flight id, should be changed later")

            image = self.picam.read()
            rotated_image = imutils.rotate(image, angle=180)
            self.sender.send_image(flight_id, rotated_image)
else: # Create a mocked image stream
    import random

    import cv2
    
    class ImageStream:
        def __init__(self, image_server=config.IMAGE_SERVER_URL):
            self.sender = imagezmq.ImageSender(connect_to=image_server)  
            logger.critical(F"Creating a mocked ImageStream")

        def take_and_upload_photo(self, flight_id="test"):
            if flight_id == "test":
                logger.warning("Using default flight id, should be changed later")

            mock_images = os.listdir('mock/images')
            image = cv2.imread(f'mock/images/{random.choice(mock_images)}')
            self.sender.send_image(flight_id, image)

        

if __name__ == "__main__":
    logger.info("Taking and sending a test photo")
    image_stream = ImageStream()
    image_stream.take_and_upload_photo()
