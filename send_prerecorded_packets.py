from packets_sender import serverAddressPort, UDPClientSocket
import logging
import time
import os

PRERECORDED_FLIGHT_DIR = "packets"

for packet_filename in os.listdir(PRERECORDED_FLIGHT_DIR):
    with open(PRERECORDED_FLIGHT_DIR + "/" + packet_filename, "rb") as packet_file:

        contents = packet_file.read()
        UDPClientSocket.sendto(contents, serverAddressPort)
        time.sleep(1)
