import os

WEB_SERVER_URL = os.environ['OVERRIDE_SERVER_URL'] if 'OVERRIDE_SERVER_URL' in os.environ else "vps-e584bd76.vps.ovh.net"

API_SERVER_URL = f"http://{WEB_SERVER_URL}:8000"
IMAGE_SERVER_URL = f"tcp://{WEB_SERVER_URL}:5555"
TELEMETRY_SERVER_URL = (WEB_SERVER_URL, 9999)

# (320, 240) is the default value, from
# https://github.com/jrosebr1/imutils/blob/master/imutils/video/videostream.py
VIDEO_RESOLUTION = (1280, 480)
