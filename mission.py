from dronekit import LocationGlobalRelative, LocationGlobal, VehicleMode, connect, Command
from packets_sender import DroneTelem
from pymavlink import mavutil
import time
import requests
import json
import logging
import math

logger = logging.getLogger(__name__)


def get_distance_metres(aLocation1, aLocation2):
    """
    Returns the ground distance in metres between two LocationGlobal objects.

    This method is an approximation, and will not be accurate over large distances and close to the 
    earth's poles. It comes from the ArduPilot test code: 
    https://github.com/diydrones/ardupilot/blob/master/Tools/autotest/common.py
    """
    dlat = aLocation2.lat - aLocation1.lat
    dlong = aLocation2.lon - aLocation1.lon
    return math.sqrt((dlat*dlat) + (dlong*dlong)) * 1.113195e5



def distance_to_current_waypoint(vehicle):
    """
    Gets distance in metres to the current waypoint. 
    It returns None for the first waypoint (Home location).
    """
    nextwaypoint = vehicle.commands.next
    if nextwaypoint==0:
        return None
    missionitem=vehicle.commands[nextwaypoint-1] #commands are zero indexed
    lat = missionitem.x
    lon = missionitem.y
    alt = missionitem.z
    targetWaypointLocation = LocationGlobalRelative(lat,lon,alt)
    distancetopoint = get_distance_metres(vehicle.location.global_frame, targetWaypointLocation)
    return distancetopoint


def add_mission(vehicle, segments):
     
    cmds = vehicle.commands

    logger.info("Clear any existing commands")
    cmds.clear()

    CLIMB_SPEED_TYPE = 2.0
    climb_speed = 15.0
    take_off_altitude = 15.0
    cmds.add(Command( 0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED, 0, 0, CLIMB_SPEED_TYPE, climb_speed, 0.0, 0.0, 0, 0, 0))
    cmds.add(Command( 0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_TAKEOFF, 0, 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, take_off_altitude))


    for way_point in segments:
        ground_speed = float(way_point['ground_speed'])
        lat =  way_point['end_position']['lat']
        lng = way_point['end_position']['lng']
        alt = float(way_point['altitude'])
        cmds.add(Command( 0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_DO_CHANGE_SPEED, 0, 0, 0.0, ground_speed, 0.0, 0.0, 0, 0, 0))
        cmds.add(Command( 0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0.0, 0.0, 0.0, 0.0, lat, lng, alt))

    dummy_waypoint = segments[-1]
    
    cmds.add(Command( 0, 0, 0, mavutil.mavlink.MAV_FRAME_GLOBAL_RELATIVE_ALT, mavutil.mavlink.MAV_CMD_NAV_WAYPOINT, 0, 0, 0.0, 0.0, 0.0, 0.0, dummy_waypoint['end_position']['lat'], dummy_waypoint['end_position']['lng'], float(dummy_waypoint['altitude'])))
    logging.info("Upload new commands")
    cmds.upload()

