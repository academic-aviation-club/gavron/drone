from __future__ import print_function

import argparse
import datetime
import json
import logging
import os
import socket
import struct
import threading
import time
from datetime import datetime, timedelta

# Set up option parsing to get connection string
import coloredlogs
import imagezmq
import requests
from dronekit import LocationGlobalRelative, VehicleMode, connect

import config
from arming import arm_and_takeoff
from image_stream import ImageStream
from mission import add_mission, distance_to_current_waypoint
from packets_sender import DroneTelem


coloredlogs.install(level="INFO")
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser(
    description="Commands vehicle using vehicle.simple_goto."
)
parser.add_argument(
    "--connect",
    help="Vehicle connection target string. If not specified, SITL automatically started and used.",
)
args = parser.parse_args()


# import datetime
connection_string = args.connect
sitl = None

# Start SITL if no connection string specified
if not connection_string:
    from ardupilot_sitl_docker.sitl import SitlDockerHelper

    sitl = SitlDockerHelper("ArduCopter", run_in_background=True)
    sitl.run()
    connection_string = "localhost:14551"

# Connect to the Vehicle
logger.info("Connecting to vehicle on: %s" % connection_string)
vehicle = connect(connection_string, wait_ready=True, baud=57600)

machine_id = 1
flight_id = int(time.time())
telemetry = DroneTelem(machine_id, flight_id)
def fly(route):
    logger.info("Create a new mission (for current location)")
    add_mission(vehicle, route)

    image_stream = ImageStream()
    # From Copter 3.3 you will be able to take off using a mission item. Plane must take off using a mission item (currently).

    arm_and_takeoff(vehicle, 15, telemetry)

    logger.info("Starting mission")
    # Reset mission set to first (0) waypoint
    vehicle.commands.next = 0

    # Set mode to AUTO to start mission
    vehicle.mode = VehicleMode("AUTO")


    # logger.info(f'couuuuuunt:  {vehicle.commands.count} and the current one: {vehicle.commands.next}')
    while True:
        nextwaypoint = vehicle.commands.next
        logger.info(
            f"Distance to waypoint {nextwaypoint}: {distance_to_current_waypoint(vehicle)}"
        )

        if nextwaypoint == vehicle.commands.count:  # Dummy waypoint
            logger.info("Exit mission")
            break
        try:
            telemetry.send_udp_packet(vehicle)
            image_stream.take_and_upload_photo()
        except Exception as e:
            logging.error(e)
        time.sleep(1)

    logger.info("Returning to Launch")
    vehicle.mode = VehicleMode("RTL")

    while True:
        time.sleep(1)
        telemetry.send_udp_packet(vehicle)

        if vehicle.armed == False:
            break

    # Close vehicle object before exiting script
    # logger.info("Close vehicle object")
    # vehicle.close()

    # # Shut down simulator if it was started.
    # if sitl:
    #     sitl.stop()


def nearest_flight():
    dates_and_routes = get_dates()
    items = list(dates_and_routes.keys())
    pivot = datetime.now()
    nearest_date = min(items, key=lambda x: abs(x - pivot))
    nearest_route_id = dates_and_routes[nearest_date]
    return nearest_date, nearest_route_id


def get_route_by_id(route_id):
    route = requests.get(f"{config.API_SERVER_URL}/route/{route_id}")
    json_route = json.loads(route.json()["drone_route"])
    segments = json_route["segments"]
    return segments


def get_schedules():
    return requests.get(f"{config.API_SERVER_URL}/flight-schedule/")


def get_dates():
    schedules = get_schedules().json()
    dates_and_routes = {}

    for i in schedules:
        date_of_flight = i["schedule_start_date"]
        route_id = i["route"]
        date_of_flight_obj = datetime.strptime(date_of_flight, "%Y-%m-%dT%H:%M:%SZ")
        dates_and_routes[date_of_flight_obj] = route_id
    return dates_and_routes


def ready_to_fly(nearest_flight):
    if (nearest_flight <= datetime.now()) and (
            nearest_flight >= datetime.now() - timedelta(seconds=10)):
            return True
    return False


if __name__ == "__main__":
    logger.info("Starting")

    while True:
        nearest_date, route_id = nearest_flight()
        logger.info(f"nearest flight at {nearest_date}, currently it is {datetime.now()}")
        logger.info(f"waiting........ {nearest_date - datetime.now()}")
        telemetry.send_udp_packet(vehicle)
        if ready_to_fly(nearest_date) or 'NOWAIT' in os.environ:
            logger.info("Create a new mission (for current location)")
            route = get_route_by_id(route_id)
            fly(route)
            vehicle.close()
            exit(0)


        time.sleep(1)
