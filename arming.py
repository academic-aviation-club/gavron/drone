from dronekit import LocationGlobalRelative, VehicleMode, connect
import logging
import coloredlogs
import time
from packets_sender import DroneTelem

coloredlogs.install(level="INFO")
logger = logging.getLogger(__name__)

def arm_and_takeoff(vehicle, aTargetAltitude, telemetry): 
    """
    Arms vehicle and fly to aTargetAltitude.
    """
    logger.info("Basic pre-arm checks")
    # Don't try to arm until autopilot is ready
    while not vehicle.is_armable:
        logger.info(" Waiting for vehicle to initialise...")
        time.sleep(1)

    logger.info("Arming motors")
    # Copter should arm in GUIDED mode
    vehicle.mode = VehicleMode("GUIDED")
    vehicle.armed = True

    # Confirm vehicle armed before attempting to take off
    while not vehicle.armed:
        logger.info(" Waiting for arming...")
        time.sleep(1)

    logger.info("Taking off!")
    vehicle.simple_takeoff(aTargetAltitude)  # Take off to target altitude

    # Wait until the vehicle reaches a safe height before processing the goto
    #  (otherwise the command after Vehicle.simple_takeoff will execute
    #   immediately).
    while True:
        logger.info(f"Altitude: {vehicle.location.global_relative_frame.alt}")
        telemetry.send_udp_packet(vehicle)
        # Break and return from function just below target altitude.
        if vehicle.location.global_relative_frame.alt >= aTargetAltitude * 0.95:
            logger.info("Reached target altitude")
            break
        time.sleep(1)
