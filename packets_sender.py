import logging
import socket
import time

import coloredlogs
from dronekit import LocationGlobalRelative, VehicleMode, connect
from google import protobuf

import config
from telemetry_definitions.generated.definitions_pb2 import GavronTelemFrame

coloredlogs.install(level="INFO")
logger = logging.getLogger(__name__)

serverAddressPort = config.TELEMETRY_SERVER_URL
UDPClientSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)


class DroneTelem:
    def __init__(self, machine_id, flight_id):
        self.machine_id = machine_id
        self.flight_id = flight_id
        self.packet_num = 0

    def send_udp_packet(self, vehicle):
        logger.info(f"SENDING PACKET FROM FLIGHT: {self.flight_id}")
        frame = GavronTelemFrame()
        frame.header.timestamp = int(time.time())
        frame.header.machine_id = self.machine_id
        frame.header.flight_id = self.flight_id

        frame.header.position.alt = vehicle.location.global_relative_frame.alt
        frame.header.position.lat = vehicle.location.global_relative_frame.lat
        frame.header.position.lng = vehicle.location.global_relative_frame.lon
        frame.header.position.heading = vehicle.heading
        frame.drone_monitoring_message.battery_left = vehicle.battery.level

        try:
            UDPClientSocket.sendto(frame.SerializeToString(), serverAddressPort)
        except Exception as e:
            logger.error(e)
        self.packet_num += 1
        # Uncomment to save packets
        # with open(f'packets/packet_{self.packet_num}', 'wb') as packet_file:
        #     packet_file.write(frame.SerializeToString())
