FROM wnt3rmute/archlinux-with-goodies:docker

RUN pacman -Syu glu --noconfirm

# create root directory for our project in the container
RUN mkdir /gavron_drone

# Set the working directory to /backend_service
WORKDIR /gavron_drone

COPY requirements.txt /gavron_drone

# Install any needed packages specified in requirements.txt
RUN pip install wheel
# WORKAROUND: removes the requirements.txt line where picamera is located
RUN awk '$1!="picamera==1.13"' requirements.txt > requirements_without_picamera.txt
RUN pip install -r requirements_without_picamera.txt 

# Copy the current directory contents into the container at /backend_service
ADD . /gavron_drone


CMD MOCK_CAMERA=1 python -u main.py
